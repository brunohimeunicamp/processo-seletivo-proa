import json
import base64
import os.path
from email.mime.text import MIMEText
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials


SCOPES = ['https://mail.google.com/']

creds = None
if os.path.exists('auth/token.json'):
    creds = Credentials.from_authorized_user_file('auth/token.json', SCOPES)

if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file('auth/credentials.json', SCOPES)
        creds = flow.run_local_server(port=0)
    with open('auth/token.json', 'w') as token:
        token.write(creds.to_json())




def create_message(to, subject, message_text):
    message = MIMEText(message_text, 'html')
    message['to'] = to
    message['subject'] = subject
    return {'raw': base64.urlsafe_b64encode(message.as_string().encode()).decode()}


def send_email(receiver: str, subject: str, message: str) -> dict:
    """Envia um email para o usuário.

    Args:
        receiver (str): Email do destinatário
        subject (str): Assunto do email
        message (str): Mensagem do email

    Returns:
        dict: Retorna um dicionário contendo o id, threadId, labelIds da mensagem.
    """    
    service = build('gmail', 'v1', credentials=creds)
    message = create_message(receiver, subject, message)

    try:
        message = (service.users().messages().send(userId='me', body=message)
                   .execute())
        print('Message Id: %s' % message['id'])
        return message
    except Exception as error:
        print(error)


def user_validation(username: str, password: str) -> bool:
    """Esta função faz uma validação do usuário e senha. Vale ressaltar que 
        esta função não faz devidamente uma validação devido a proposta do protótipo estilo MVP. 
        O correto seria integrar com um banco de dados como MYSQL hospedado em algum serviço de nuvem como 
        Amazon RDS na AWS ou Google Cloud Platform, e finalmente criptografar a senha com algum algoritmo de 
        hashing para a proteção dos dados do usuário, este último poderia ser possível a partir do modulo bcrypt.

    Args:
        username (str): usuário
        password (str): senha

    Returns:
        bool: True se o usuário e senha forem válidos.
    """    
    return False

def check_existing_user(cpf: str) -> bool:
    """Esta função verifica se o usuário já existe no banco de dados teórico a partir do CPF.

    Args:
        cpf (str): CPF do usuário

    Returns:
        bool: Retorna False caso o usuário não exista no banco de dados.
    """    
    return False

def update_db(user_data: dict) -> bool:
    """Esta função irá atualizar o banco de dados com os dados do usuário.
        Pelo mesmo motivo da função user_validation, esta função não faz devidamente
        uma atualização devido a proposta do protótipo estilo MVP.

    Args:
        user_data (dict): dicionário contendo os dados dos usuários

    Returns:
        bool: Retorna true caso os dados sejam atualizados com sucesso.
    """    
    return True


def read_json(filename: str) -> dict:
    """Esta função lê um arquivo json e retorna um dicionário.

    Args:
        filename (str): caminho do arquivo json

    Returns:
        dict: retorna um dicionário com os dados do arquivo json.
    """    
    with open(filename, encoding='utf-8') as f:
        return json.load(f)
    



