from flask import Flask, render_template, request, redirect, session, url_for
from functions import send_email, user_validation, check_existing_user, update_db, read_json
import datetime
import random
app = Flask(__name__)

app.secret_key = 'plataforma_proa'
app.config['PERMANENT_SESSION_LIFETIME'] =  datetime.timedelta(minutes=120)

@app.route('/', methods=['GET', 'POST'])
def login():
    if 'username' in session:
        return redirect(url_for('questions'))
    
    if request.method == 'POST':
        username = request.form.get("username")
        password = request.form.get("password")
        if user_validation(username, password):
            session['username'] = username
            return redirect(url_for('questions'))
        

    current_year = datetime.datetime.now().year
    return render_template('login.html', year=current_year)

@app.route('/forms', methods=['GET', 'POST'])
def forms():
    if 'username' in session:
        return redirect(url_for('questions'))
    
    if request.method == 'POST':
        user_data = {
            "username": request.form.get("username"),
            "surname": request.form.get("surname"),
            "email": request.form.get("email"),
            "password": request.form.get("password"),
            "cpf": request.form.get("cpf"),
            "whatsapp": request.form.get("whatsapp"),
            "date": request.form.get("date"),
            "state": request.form.get("state"),
            "city": request.form.get("city"),
            "school": request.form.get("school"),
            "school_level": request.form.get("school-level"),
            "work_hours": request.form.get("work_hours"),
            "study_hours": request.form.get("study_hours"),
            "daily_free_time": request.form.get("daily_free_time"),
            "marital_status": request.form.get("marital_status"),
            "has_children": request.form.get("has_children"),
            "num_household_members": request.form.get("num_household_members"),
            "online_course_experience": request.form.get("online_course_experience"),
            "stable_internet": request.form.get("stable_internet"),
            "job_timeline": request.form.get("job_timeline"),
            "quiet_study_environment": request.form.get("quiet_study_environment")
        }

        if not check_existing_user(user_data["cpf"]):

            session['username'] = user_data["username"]
            session['email'] = user_data["email"]
            update_db(user_data)
            
            message = f"""
            <html>
            <head></head>
            <body>
            <p>Olá, {user_data['username']}!</p>
            <p>A primeira etapa do seu cadastro foi realizada com sucesso! Seu login para acessar a plataforma é:</p>
            <ul>
                <li>Usuário: {user_data['cpf']}</li>
                <li>Senha: {user_data['password']}</li>
            </ul>
            <p>Lembre-se de que a inscrição ainda não foi finalizada. Você precisa responder algumas perguntas para concluir seu cadastro. Essas perguntas estão no mesmo link que você usou para preencher suas informações pessoais. Se tiver algum problema, não hesite em entrar em contato conosco.</p>
            <p>Atenciosamente,</p>
            <p>Equipe Proa.</p>
            </body>
            </html>
            """
            send_email(user_data["email"], "Continue sua inscrição no Proa!!" , message)
            
            return redirect(url_for('questions'))
    
    current_year = datetime.datetime.now().year
    return render_template('forms.html', year=current_year)

@app.route('/questions', methods=['GET', 'POST'])
def questions():
    if "username" in session:
        error = False
        if "page_number" not in session:
            session['page_number'] = 1

        if "modules_completed" not in session:
            # Dicionário com string devido como Flask trata suas sessions
            session['modules_completed'] = {
                "1": False,
                "2": False,
                "3": False,
                "4": False,
            }

        if request.method == 'POST':
            if request.form.get("next-page") == "Próxima página":
                session['page_number'] += 1
            elif request.form.get("previous-page") == "Página anterior":
                session['page_number'] -= 1
            elif request.form.get("check") == "Checar resposta":
                user_answers = list()
                for i in range(1, 7):
                    user_answers.append(request.form.get(f"question_{i}"))
                if "wrong" in user_answers:
                    # Escolhe 6 novas questões para o usuário responder
                    # Ao apagar a session, o código irá buscar novamente do arquivo json
                    session.pop(f"shuffled_questions_{session['page_number']}", None)
                    error = True
                else:
                    temporary_dict = session['modules_completed'].copy()
                    temporary_dict[str(session['page_number'])] = True
                    session['modules_completed'] = temporary_dict
        
            # Previne que o usuário acesse páginas que não existem, caso 
            # caso o código entre em um método POST ao recarregar a página
            if session['page_number'] >= 5 or session['page_number'] <= 0:
                session['page_number'] = 1

        modules = {
            1: "first_module.json",
            2: "second_module.json",
            3: "third_module.json",
            4: "fourth_module.json",
        }

        # Escolhe aleatoriamente 6 perguntas do módulo da página atual
        if f"shuffled_questions_{session['page_number']}" not in session:
            PATH = "static/JSON_files/"
            questions = read_json(PATH + modules[session['page_number']])

            items = list(questions.items())
            random.shuffle(items)
            shuffled_questions = dict(items[:6])
            session[f"shuffled_questions_{session['page_number']}"] = shuffled_questions

            # Cria uma lista com as respostas certas
            correct_answers_list = list()
            for values in shuffled_questions.values():
                correct_answers_list.append(values['Resposta'])

            session[f"correct_answers_list_{session['page_number']}"] = correct_answers_list


        if list(session['modules_completed'].values()) == [True, True, True, True]:
            session["subscription_completed"] = True
            update_db(session["subscription_completed"])
                
            message = message = f"""
            <html>
            <head></head>
            <body>
            <p>Olá, {session['username']}!</p>
            <p>Agradecemos por se inscrever no Proa. O cadastro foi finalizado e inscrição foi realizada com sucesso! 
            Agora, você está um passo mais próximo de fazer parte da nossa comunidade. Fique atento ao seu email, 
            pois enviaremos as próximas atualizações sobre o resultado do processo seletivo.</p>
            <p>Atenciosamente,</p>
            <p>Equipe Proa</p>
            </body>
            </html>
            """
            send_email(session["email"], "Inscrição no Proa realizada com sucesso!!" , message)
            return redirect(url_for('subscription_completed'))

        current_year = datetime.datetime.now().year
        return render_template('questions.html', 
                            year=current_year,
                            page_number=session['page_number'],
                            questions=session[f"shuffled_questions_{session['page_number']}"],
                            correct_answers_list=session[f"correct_answers_list_{session['page_number']}"],
                            modules_completed=session['modules_completed'],
                            error=error
                            )
    
    return redirect(url_for('login'))



@app.route('/subscription_completed', methods=['GET', 'POST'])
def subscription_completed():
    if 'subscription_completed' in session:

        current_year = datetime.datetime.now().year 
        return render_template("subscription_completed.html", year=current_year)
        
    return redirect(url_for('login'))

if __name__ == '__main__':
    app.run(debug=True)
